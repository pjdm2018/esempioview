package it.uniroma2.pjdm.esempioview1;

import android.content.Context;
import android.graphics.Canvas;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;

/**
 * Created by clauz on 5/6/18.
 */

public class MyButtonView extends Button {

    final private String myTAG = "MyButtonView";

    /* right click generate constructor */
    public MyButtonView(Context context) {
        super(context);
        Log.d(myTAG, "constructor 1");
    }

    public MyButtonView(Context context, AttributeSet attrs) {
        super(context, attrs);
        Log.d(myTAG, "constructor 2");
    }

    public MyButtonView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        Log.d(myTAG, "constructor 3");
    }

    public MyButtonView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        Log.d(myTAG, "constructor 4");
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        Log.d(myTAG, "onMeasure " + widthMeasureSpec  + " --> " + View.MeasureSpec.toString(widthMeasureSpec));
        Log.d(myTAG, "onMeasure " + heightMeasureSpec  + " --> " + View.MeasureSpec.toString(heightMeasureSpec));
        int width  = MeasureSpec.getSize(widthMeasureSpec);
        int height = MeasureSpec.getSize(heightMeasureSpec);
        if(MeasureSpec.getMode(widthMeasureSpec) == MeasureSpec.AT_MOST &&
                MeasureSpec.getMode(heightMeasureSpec) == MeasureSpec.AT_MOST)
            setMeasuredDimension(width, height);
    }

    @Override
    protected void onLayout(boolean changed, int left, int top, int right, int bottom) {
        super.onLayout(changed, left, top, right, bottom);
        Log.d(myTAG, "onLayout " + changed + " " + left + " " + top + " " + right + " " + bottom);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        Log.d(myTAG, "onDraw");
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        Log.d(myTAG, "onTouch " + event);
        /* returning false passes the event to the layout */
        return false;
        // return super.onTouchEvent(event);
    }
}
