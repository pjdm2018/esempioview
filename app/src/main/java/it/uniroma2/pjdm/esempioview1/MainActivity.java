package it.uniroma2.pjdm.esempioview1;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;

public class MainActivity extends AppCompatActivity {
    final private String myTAG = "MainActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        findViewById(R.id.myButtonView).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.d(myTAG, "OK clicked");
            }
        });

        findViewById(R.id.myLayout).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.d(myTAG, "layout clicked");
            }
        });

        findViewById(R.id.myButtonView).setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                Log.d(myTAG, "OK long clicked");
                return false;
            }
        });

        findViewById(R.id.myLayout).setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                Log.d(myTAG, "layout long clicked");
                return true;
            }
        });

        findViewById(R.id.myButtonView).setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                Log.d(myTAG, "onTouch " + motionEvent);
                /* this is called before onTouchEvent() */
                /* setting this to true will consume the event and not pass it to onTouchEvent()*/
                return false;
            }
        });

    }

}
