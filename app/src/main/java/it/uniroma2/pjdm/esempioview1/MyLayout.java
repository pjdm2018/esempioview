package it.uniroma2.pjdm.esempioview1;

import android.content.Context;
import android.graphics.Canvas;
import android.support.constraint.ConstraintLayout;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;

/**
 * Created by clauz on 5/6/18.
 */

public class MyLayout extends ConstraintLayout {

    final private String myTAG = "MyLayout";

    public MyLayout(Context context) {
        super(context);
        Log.d(myTAG, "layout constructor 1");
    }

    public MyLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
        Log.d(myTAG, "layout constructor 2");
    }

    public MyLayout(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        Log.d(myTAG, "layout constructor 3");
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        Log.d(myTAG, "widthMeasureSpec: " + MeasureSpec.toString(widthMeasureSpec));
        Log.d(myTAG, "heightMeasureSpec: " + MeasureSpec.toString(heightMeasureSpec));
    }

    @Override
    protected void onLayout(boolean changed, int left, int top, int right, int bottom) {
        super.onLayout(changed, left, top, right, bottom);
        Log.d(myTAG, "onLayout: " + changed + " " + left + " " + top + " " + right + " " + bottom);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        Log.d(myTAG, "onDraw");
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        Log.d(myTAG, "onTouch " + event);
        return super.onTouchEvent(event);
    }
}
